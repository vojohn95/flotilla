<?php
include $_SERVER['DOCUMENT_ROOT'].'/arrendamiento/conexion/conexion.php';
?>
<!DOCTYPE html><html lang="es" >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">
    <title>Flotilla</title>
</head>
<body>
<?php
$mensaje = htmlentities($_GET['msj']);
$c = htmlentities($_GET['c']);
$p = htmlentities($_GET['p']);
$t = htmlentities($_GET['t']);
//


switch ($c) {
    case 'fo':
        $carpeta = '../inicio/';
        break;
    case 'us':
        $carpeta = '../usuarios/';
        break;
    case 'home':
        $carpeta = '../inicio/';
        break;
    case 'salir':
        $carpeta = '../';
        break;

}//termina switch

switch ($p) {
    case 'tos':
        $pagina = 'consultarcoches.php';
        break;
    case 'in':
        $pagina = 'index.php';
        break;
    case 'admin':
        $pagina = 'indexadmin.php';
        break;
    case 'activos':
        $pagina = 'indexactivos.php';
        break;
    case 'dev':
        $pagina = 'index.php';
        break;
    case 'salir':
        $pagina = '';
        break;

}//termina segundo switch

$dir = $carpeta.$pagina;
if ($t == "error") {
    $titulo = "Oppss...";
}else{
    $titulo = "Buen trabajo";
}

?>

<script
    src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script>
    swal({
        title: '<?php echo $titulo ?>',
        text: '<?php echo $mensaje ?>',
        type: '<?php echo $t ?>',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'ok'
    }).then(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).click(function(){
        location.href='<?php echo $dir ?>';
    });

    $(document).keyup(function(e){
        if (e.which == 27) {
            location.href='<?php echo $dir ?>';
        }
    });

</script>

</body>
</html>
