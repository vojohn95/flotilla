<?php
include '../Conexion/conexion.php';
//evita que se reabra la sesion una vez cerrado la pagina
if (!isset($_SESSION['nick'])) {
    header('location: ../');
    //cuando se ingresa del login se general las variables de sesion desde el login, aqui si existe se redirecciona a inicio
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="Author" content="Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <title>Flotilla OCE</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="icon" href="../img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../img/favicon/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="../img/favicon/mstile-144x144.png">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../css/gallery-materialize.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.css" rel="stylesheet">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/mdb.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <div class="loader"></div>
    <style>.loader{position:fixed;left:0;top:0;width:100%;height:100%;z-index:9999;background:url('../img/loader.gif') 50% 50% no-repeat #f9f9f9;opacity:.8}</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">$(window).load(function(){$(".loader").fadeOut("slow")});</script>
</head>

        <!--Navbar -->
        <nav class="mb-4 navbar navbar-expand-lg navbar-dark black">
            <a class="navbar-brand font-bold" href=""><img src="../img/logo/login-logo.png" style="width: 50px;" id="cam1"> Central Operativo de Estacionamientos</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                <ul class="navbar-nav ml-auto">
                    <?php
                    //Discrimina para asignar los accesos al login

                    //AGREGA LOS IFS PARA LOS MODULOS menu_MODULO!.php
                    if ($_SESSION['nivel'] == 'developer') {
                        include 'menu_developer.php';
                    } elseif ($_SESSION['nivel'] == 'activos') {
                        include 'menu_activos.php';
                    }elseif ($_SESSION['nivel'] == 'administrador') {
                        include 'menu_admin.php';
                    } else{
                        echo "Ingreso incorrecto";
                    }
                    //error_reporting(0);
                    ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Cerrar sesión </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-cyan" aria-labelledby="navbarDropdownMenuLink-4">
                            <a class="dropdown-item" href="../login/Salir.php">Salir</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <!--/.Navbar -->



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.1/js/mdb.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="https://cdn.jsdelivr.net/materialize/0.98.0/js/materialize.min.js"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.js"></script>
<script src="js/color-thief.min.js"></script>
<script src="js/galleryExpand.js"></script>
<script src="js/init.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
