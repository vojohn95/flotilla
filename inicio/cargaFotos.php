<?php
include '../extend/header.php';
$id = htmlentities($_GET['id']);



?>

<div class="md-form" >
    <div class="card">
        <div class="card-header" style="border-bottom-color: #00A88E; border-bottom-width: medium; background-color: black;">
            <h5 style="color: white;" align="center">CARGA DE FOTOS</h5>
        </div>
        <div class="card-body">
            <br>
            <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
            <form action="recibeFotos.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <label id="seleccionarTodo" for="selectFile" style="padding-left: 30px;">Seleccionar fotos a cargar</label>
                <input id="fotos[]" name="fotos[]" type="file" accept="image/*" multiple style="padding-left: 50px;"/>
                <button type="submit" name="submit" class="btn btn-primary">Cargar</button>
            </form>
        </div>
    </div>
</div>
