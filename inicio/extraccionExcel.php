<?php
include '../conexion/conexion.php';
include '../composer/vendor/autoload.php';
echo $usuario = $_SESSION['id'];

ini_set('memory_limit', '-1');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

if (isset($_FILES['file']['name']) && in_array($_FILES['file']['type'], $file_mimes)) {

    $arr_file = explode('.', $_FILES['file']['name']);
    $extension = end($arr_file);

    if ('csv' == $extension) {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    } else {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    }

    $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
    $spreadsheet->getSheetCount();
    //$hojaActual = $spreadsheet->getSheet(0);
    $sheetData = $spreadsheet->getSheet(0)->toArray();
    //print_r($sheetData);
    foreach ($sheetData as $key => $value) {
        //for ($value = 1; $value = $sheetData; $++){
        ?>

        <table border="5">

            <?php

            if ($value[0] == 'USUARIO') {
                continue;
            } else {
                $usuarioAuto = $value[0];
            }

            if ($value[1] == 'AUTO') {
                continue;
            } else {
                $auto = $value[1];
            }

            if ($value[2] == 'MARCA') {
                continue;
            } else {
                $marca = $value[2];
            }

            if ($value[3] == 'AÑO') {
                continue;
            } else {
                $ano = $value[3];
            }

            if ($value[4] == 'COLOR') {
                continue;
            } else {
                $color = $value[4];
            }

            if ($value[5] == 'PLACAS') {
                continue;
            } else {
                $placas = $value[5];
            }

            if ($value[6] == 'ENTIDAD') {
                continue;
            } else {
                $entidad = $value[6];
            }

            if ($value[7] == 'FECHA') {
                continue;
            } else {
                $fecha = $value[7];
            }

            if ($value[8] == 'SERVICIO') {
                continue;
            } else {
                $servicio = $value[8];
            }

            if ($value[9] == 'KILOMETRAJE') {
                continue;
            } else {
                $kilometraje = $value[9];
            }

            if ($value[10] == 'PLAN DE MANTENIMIENTO') {
                continue;
            } else {
                $planmantenimiento = $value[10];
            }

            if ($value[11] == 'GARANTÍA 3 AÑOS') {
                continue;
            } else {
                $garantia = $value[11];
            }

            if ($value[12] == 'AGENCIA / TALLER') {
                continue;
            } else {
                $agencia = $value[12];
            }

            $insert_excel = "INSERT INTO autos (EmployeerID , Auto, Marca, Año, Color, Placas, Entidad,  Fecha_Ingreso, Kilometraje, Plan_Mantenimiento, Activo) VALUES
            ('" . $usuario . "','" . $auto . "','" . $marca . "','" . $ano . "','" . $color . "','" . $placas . "' ,'" . $entidad . "' ,'" . $fecha . "' ,'" . $kilometraje . "','" . $planmantenimiento . "', 1)";
            //echo $insert_excel;
            $sel = "SELECT MAX(CarID) FROM autos";
            $consulta = mysqli_query($mysqli, $sel);
            $f = mysqli_fetch_assoc($consulta);
            $maximo = $f['MAX(CarID)'] + 1;
            if (mysqli_query($mysqli, $insert_excel)) {
                // echo "empieza";
                //echo $maximo;
                $insert_excel2 = "INSERT INTO servicio (CarID, Fecha_Servicio, Garantia, Agencia_Taller) VALUES
                ('" . $maximo . "','" . $servicio . "','" . $garantia . "' ,'" . $agencia . "')";
                //echo $insert_excel2;
                if (mysqli_query($mysqli, $insert_excel2)) {
                    $insert_excel3 = "INSERT INTO usuarios (UserID, Nombre, Activo) VALUES
                ('" . $maximo . "','" . $usuarioAuto . "', 1)";
                    if (mysqli_query($mysqli, $insert_excel3)) {
                        $insert_excel4 = "INSERT INTO asignados (AsigID, UserID, CarID) VALUES
                       ('" . $maximo . "','" . $maximo . "','" . $maximo . "')";
                        if (mysqli_query($mysqli, $insert_excel4)) {
                            header('location: ../extend/alerta.php?msj=Excel cargado correctamente!&c=home&p=activos&t=success');
                        }
                        //echo $insert_excel3;
                        //echo "hecho";
                        // header('location: ../extend/alerta.php?msj=Excel cargado correctamente!&c=home&p=activos&t=success');
                    }
                    // header('location: ../extend/alerta.php?msj=Excel cargado correctamente!&c=home&p=activos&t=success');
                } else {
                    //print_r($mysqli->error_list);
                    header('location: ../extend/alerta.php?msj=Error al cargar su excel!&c=home&p=activos&t=error');
                }
            } else {
                header('location: ../extend/alerta.php?msj=Ocurrio un error al cargar&c=home&p=activos&t=error');
            }

            ?>


        </table>

        <?php

    }
}

?>
