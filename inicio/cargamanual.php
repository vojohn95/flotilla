<?php
include '../extend/header.php';
?>

<!-- Material form subscription -->
<div class="card">

    <h5 class="card-header info-color white-text text-center py-4">
        <strong>Ingrese la información del auto en la sección de abajo</strong>
    </h5>

    <!--Card content-->
    <div class="card-body px-lg-5">

        <form class="needs-validation" action="insercioncm.php" method="GET" autocomplete="off" validate>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationTooltip01">Auto</label>
                    <input type="text" class="form-control" id="auto" name="auto" placeholder="Ejemplo: NISSAN URVAN">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationTooltip02">Marca</label>
                    <input type="text" class="form-control" id="marca" name="marca" placeholder="Ejemplo: NISSAN">
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationTooltipUsername">Año</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="ano" name="ano" placeholder="Ejemplo: 2019">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationTooltip03">Usuario</label>
                    <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Agregue al usuario que maneja el auto">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip04">Color del auto</label>
                    <input type="text" class="form-control" id="color" name="color" placeholder="Color">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip05">Placas</label>
                    <input type="text" class="form-control" id="placas" name="placas" placeholder="Placas" >
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationTooltip03">Entidad</label>
                    <input type="text" class="form-control" id="entidad" name="entidad" placeholder="Agregue su entidad federativa">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip04">Fecha ingreso</label>
                    <input type="date" class="form-control" id="fecha_ingreso" >
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip05">Fecha de compra</label>
                    <input type="date" class="form-control" id="fecha_compra">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip03">Descripción</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" >
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip03">Kilometraje</label>
                    <input type="text" class="form-control" id="kilometraje" placeholder="kilometraje" >
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip04">Tenencia</label>
                    <input type="text" class="form-control" id="tenencia" name="tenencia" placeholder="Tenencia">
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationTooltip05">Plan Mantenimiento</label>
                    <input type="text" class="form-control" id="Pm" name="Pm" placeholder="Plan Mantenimiento" >
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Subir</button>
        </form>

    </div>

</div>
    </div>
<!-- Material form subscription -->
<?php include '../extend/footer.php'; ?>