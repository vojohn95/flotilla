<?php
include '../extend/header.php';

//$con = "SELECT * FROM autos LIMIT 1,20";
$con = "SELECT * FROM autos 
INNER JOIN servicio ON autos.CarID = servicio.CarID 
INNER JOIN asignados ON servicio.CarID = asignados.CarID 
INNER JOIN usuarios ON usuarios.UserID = asignados.UserID 
ORDER BY asignados.UserID ASC ";
$consulta = mysqli_query($mysqli, $con) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);

?>

    <div class="row">
        <?php while ($f = mysqli_fetch_assoc($consulta)) { ?>
            <!--inicia row-->
            <div class="col" style="padding-top: 20px; padding-left: 30px; padding-right: 20px;">
                <div class="card hoverable" style="width: 18rem;">
                    <!--Carousel Wrapper-->
                    <div id="carousel-example-1z_<?php echo $f['UserID'] ?>" class="carousel slide carousel-fade"
                         data-ride="carousel">

                        <?php
                        $con2 = "SELECT * FROM fotos WHERE CarID = " . $f['CarID'];
                        $consulta2 = mysqli_query($mysqli, $con2) or die('Error al buscar en la base de datos.');
                        $row2 = mysqli_num_rows($consulta2); ?>
                        <!--Indicators-->
                        <ol class="carousel-indicators">
                            <?php if ($row2 <= 0) { ?>
                                <li data-target="#carousel-example-1z_<?php echo $f['UserID'] ?>"
                                    data-slide-to="0"></li>
                                <li data-target="#carousel-example-1z_<?php echo $f['UserID'] ?>"
                                    data-slide-to="1"></li>
                            <?php } else { ?>
                                <?php for ($i = 1; $i <= $row2; $i++) { ?>
                                    <li data-target="#carousel-example-1z_<?php echo $f['UserID'] ?>"
                                        data-slide-to="<?php echo $i ?>"></li>
                                <?php }//termina for
                            } //termina else?>
                        </ol>
                        <!--/.Indicators-->


                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <!--First slide-->
                            <?php if ($row2 <= 0) { ?>
                                <div class="carousel-item active">
                                    <img class="d-block w-100"
                                         src="https://i2.wp.com/viewparking.net/wp-content/uploads/2015/05/parkinggarage1.jpg"
                                         alt="First slide" style="height: 200px;">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100"
                                         src="http://s1.1zoom.me/big0/192/BMW_M3_F80_Mode_448988.jpg"
                                         alt="Second slide" style="height: 200px;">
                                </div>

                                <!--/First slide-->
                            <?php } else {
                                $bool = true;
                                while ($c = mysqli_fetch_assoc($consulta2)) {
                                    if ($bool) { ?>
                                        <div class="carousel-item active">
                                            <?php $foto = $c['Foto'] ?>
                                            <img class="d-block w-100"
                                                 src="archivos/<?php echo $foto ?> " style="height: 200px;">
                                        </div>
                                        <?php
                                        $bool = false;
                                    } else { ?>
                                        <!--Second slide-->
                                        <div class="carousel-item">
                                            <?php $foto = $c['Foto'] ?>
                                            <img class="d-block w-100"
                                                 src="archivos/<?php echo $foto ?>" style="height: 200px;">
                                        </div>
                                    <?php } ?>
                                    <!--/Second slide-->
                                <?php }
                            } ?>
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-1z_<?php echo $f['UserID'] ?>"
                           role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-1z_<?php echo $f['UserID'] ?>"
                           role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                    <!--/.Carousel Wrapper-->
                    <div class="card-body">
                        <?php $f['UserID']; ?>
                        <h5 class="card-title"><?php echo $f['Nombre']; ?></h5>
                        <p class="card-text">
                        <div class="row">

                        </div>
                        </p>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#basicExampleModal_<?php echo $f['UserID'] ?>">
                            Visualizar datos
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="basicExampleModal_<?php echo $f['UserID'] ?>" tabindex="-1"
                             role="dialog" aria-labelledby="exampleModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Información del auto:
                                            <br> <?php echo $f['Nombre'] ?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Auto</th>
                                                    <th scope="col">Marca</th>
                                                    <th scope="col">Año</th>
                                                    <th scope="col">Color</th>
                                                    <th scope="col">Placas</th>
                                                    <th scope="col">Entidad</th>
                                                    <th scope="col">Fecha ingreso</th>
                                                    <th scope="col">Fecha compra</th>
                                                    <th scope="col">Descripcion</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><?php echo $f['Auto'] ?></td>
                                                    <td><?php echo $f['Marca'] ?></td>
                                                    <td><?php echo $f['Año'] ?></td>
                                                    <td><?php echo $f['Color'] ?></td>
                                                    <td><?php echo $f['Placas'] ?></td>
                                                    <td><?php echo $f['Entidad'] ?></td>
                                                    <td><?php echo $f['Fecha_Ingreso'] ?></td>
                                                    <?php if ($f['Fecha_Compra'] == NULL) {
                                                        echo "<td>Información sin cargar</td>";
                                                    } else {
                                                        echo "<td>" . $f['Fecha_Compra'] . "</td>";
                                                    } ?>
                                                    <?php if ($f['Descripcion'] == NULL) {
                                                        echo "<td>Información sin cargar</td>";
                                                    } else {
                                                        echo "<td>" . $f['Descripcion'] . "</td>";
                                                    } ?>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div><!--Terminara tabla-->
                                        <!--Segunda tabla-->
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Kilometraje</th>
                                                    <th scope="col">Tenencia</th>
                                                    <th scope="col">Plan mantenimiento</th>
                                                    <th scope="col">Fecha servicio</th>
                                                    <th scope="col">Costo</th>
                                                    <th scope="col">Proveedor</th>
                                                    <th scope="col">Garantia</th>
                                                    <th scope="col">Agencia/Taller</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <?php if ($f['Kilometraje'] == NULL) {
                                                        echo "<td>Información sin cargar</td>";
                                                    } else {
                                                        echo "<td>" . $f['Kilometraje'] . "</td>";
                                                    } ?>

                                                    <?php if ($f['Tenencia'] == NULL) {
                                                        echo "<td>Información sin cargar</td>";
                                                    } else {
                                                        echo "<td>" . $f['Tenencia'] . "</td>";
                                                    } ?>
                                                    <td><?php echo $f['Plan_Mantenimiento'] ?></td>
                                                    <td><?php echo $f['Fecha_Servicio'] ?></td>
                                                    <td><?php echo $f['Costo'] ?></td>
                                                    <td><?php echo $f['Proveedor'] ?></td>
                                                    <td><?php echo $f['Garantia'] ?></td>
                                                    <td><?php echo $f['Agencia_Taller'] ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div><!--Terminara tabla-->

                                    </div>
                                    <div class="modal-footer">
                                        <!--boton de modal input file-->
                                        <!-- Button trigger modal -->
                                        <!-- Frame Modal Bottom -->
                                        <a class="btn btn-primary red"
                                           href="cargaFotos.php?id=<?php echo $f['UserID'] ?>">Cargar fotos</a>
                                        <button type="button" class="btn btn-primary">Aprobar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>
        <!--TErmina row-->
    </div>

<?php include '../extend/footer.php'; ?>