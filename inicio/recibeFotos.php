<?php
include '../conexion/conexion.php';
$id = $_POST['id'];
//echo "Cantidad de archivos: ";
$conteo = count($_FILES["fotos"]);
echo "<br>";
//Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
foreach($_FILES["fotos"]['tmp_name'] as $key => $tmp_name)
{
    //Validamos que el archivo exista
    if($_FILES["fotos"]["name"][$key]) {
        $filename = $_FILES["fotos"]["name"][$key]; //Obtenemos el nombre original del archivo
        $source = $_FILES["fotos"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo

        $directorio = 'archivos/'; //Declaramos un  variable con la ruta donde guardaremos los archivos

        //Validamos si la ruta de destino existe, en caso de no existir la creamos
        if(!file_exists($directorio)){
            mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
        }

        $dir=opendir($directorio); //Abrimos el directorio de destino
        $target_path = $directorio.'/'.$filename; //Indicamos la ruta de destino, así como el nombre del archivo

        //Movemos y validamos que el archivo se haya cargado correctamente
        //El primer campo es el origen y el segundo el destino
        $sel = "SELECT MAX(FotosID) FROM fotos";
        $consulta = mysqli_query($mysqli, $sel);
        $f = mysqli_fetch_assoc($consulta);
        $maximo = $f['MAX(FotosID)'] + 1;
        $sql = "INSERT INTO fotos (FotosID, CarID, Foto) VALUES ('" . $maximo . "', '" . $id . "', '" . $filename . "')";

        if (mysqli_query($mysqli, $sql)) {
            move_uploaded_file($source, $target_path);
            header('location: ../extend/alerta.php?msj=Fotos cargadas correctamente!&c=fo&p=tos&t=success');
            //echo "El archivo $filename se ha almacenado en forma exitosa.<br>";
            //echo "New record created successfully<br>";
        } else {
            header('location: ../extend/alerta.php?msj=Porfavor intentelo de nuevo!&c=fo&p=tos&t=error');
            //echo "Ha ocurrido un error, por favor inténtelo de nuevo.<br>";
            //echo "Error: " . $sql . "" . mysqli_error($mysqli);
        }

    }

    closedir($dir); //Cerramos el directorio de destino

}

?>
>