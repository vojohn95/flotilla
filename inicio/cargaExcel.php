<?php
include '../extend/header.php';
$id = $_SESSION['id'];
?>

<div class="md-form" >
    <div class="card">
        <div class="card-header" style="border-bottom-color: #00A88E; border-bottom-width: medium; background-color: black;">
            <h5 style="color: white;" align="center">CARGA EXCEL</h5>
        </div>
        <div class="card-body">
            <br>
            <h5 class="card-title">Escoge tu layout pulsando en el boton de abajo</h5>
            <!--<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>-->
            <form action="extraccionExcel.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
                <button type="submit" name="submit" class="btn btn-primary">Cargar</button>
            </form>
        </div>
    </div>
</div>

<script src="../js/activos.js" ></script>

<?php include '../extend/footer.php'; ?>