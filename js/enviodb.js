$( "#formManual" ).submit(function( event ) { //En el evento Submit del formulario
    var a = $("#noestacionamiento").val();
    $("#NoEstacionamientoManual").val(a);
    //alert( "Handler for .submit() called." );// mensaje de alerta solo para checar que la funcion entro a ejecutarce esto si quieres lo puedes quitar.
    event.preventDefault(); // detiene el envio por post del formulario
    var f = $(this);
    var formData = new FormData(document.getElementById("formManual")); //optiene todos los datos del formulario
    formData.append("dato", "valor"); // se asigna valor y dato
    // envio por AJAX
    $.ajax({
        url: "individual.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#resultado").html("<span>Cargando...</span><div class=\"progress md-progress primary-color-dark\">\n" +
                "    <div class=\"indeterminate\"></div>\n" +
                "</div>");
        },
        success: function (response) {

            console.log("Esto es lo que devolvio" + response);

            if (response.indexOf("¡Exito!") > -1) {

                $("#resultado").html('<div class="alert alert-success" role="alert">\n' +
                    '            Se ha enviado correctamente la información!\n' +
                    '            <p>Pronto te llegara un correo nuestro a la dirección que proporcionaste</p>\n' +
                    '        </div>');

                setInterval(function(){ $("#resultado").html(""); }, 2300);
                //alert("ENTRO");
                $("#manual input").each(function () {
                    //$(this).empty();
                    $(this).val("");
                    /*var a = $(this).val();
                    alert(a);*/
                });

            }else {
                $("#resultado").html(response);
            }

        }
    })
});

/*function getFormDataManual() {
    var config = {};
    $('#manual input, #manual select').each(function () {
        config[this.name] = this.value;
        /*var a = $(this).val();
        console.log("Contenido del Input: " + a);
    });
     config["NumeroEstacionamiento"] = $("#noestacionamiento").val();
    $.ajax({
        data: {"nombre_parametro": config}, //datos que se envian a traves de ajax
        url: 'individual.php', //archivo que recibe la peticion
        type: 'post', //método de envio
        beforeSend: function () {
            $("#resultado").html("<span>Cargando...</span><div class=\"progress md-progress primary-color-dark\">\n" +
                "    <div class=\"indeterminate\"></div>\n" +
                "</div>");
        },
        success: function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $("#resultado").html(response);
            console.log("Esto es lo que devolvio" + response);
            if(response.indexOf("¡Exito!") > -1){
                //alert("ENTRO");
                $("input").each(function () {
                    //$(this).empty();
                    $(this).val("");
                    /*var a = $(this).val();
                    alert(a);
                });
            }

        }
    });
}*/
/*
$("#automatico #formAutomatico").submit(function (e) {
    alert("se detuvo el envio de formulario");
    e.preventDefault();
});*/
$( "#formAutomatico" ).submit(function( event ) { //En el evento Submit del formulario
    var a = $("#noestacionamiento").val();
    $("#NoEstacionamientoAutomatico").val(a);
    //alert( "Handler for .submit() called." );// mensaje de alerta solo para checar que la funcion entro a ejecutarce esto si quieres lo puedes quitar.
    event.preventDefault(); // detiene el envio por post del formulario
    var f = $(this);
    var formData = new FormData(document.getElementById("formAutomatico")); //optiene todos los datos del formulario
    formData.append("dato", "valor"); // se asigna valor y dato
    // envio por AJAX
    $.ajax({
        url: "automatic.php",
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#resultado").html("<span>Cargando...</span><div class=\"progress md-progress primary-color-dark\">\n" +
                "    <div class=\"indeterminate\"></div>\n" +
                "</div>");
        },
        success: function (response) {

            console.log("Esto es lo que devolvio" + response);

            if (response.indexOf("¡Exito!") > -1) {

                $("#resultado").html('<div class="alert alert-success" role="alert">\n' +
                    '            Se ha enviado correctamente la información!\n' +
                    '            <p>Pronto te llegara un correo nuestro a la dirección que proporcionaste</p>\n' +
                    '        </div>');
                setInterval(function(){ $("#resultado").html(""); }, 2300);
                //alert("ENTRO");
                $("#automatico input").each(function () {
                    //$(this).empty();
                   $(this).val("");
                    /*var a = $(this).val();
                    alert(a);*/
                });

            }else {
                $("#resultado").html(response);
            }

        }
    })
});

